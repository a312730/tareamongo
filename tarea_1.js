//¿cuantos registros arrojo el comando count? :  800

//Encuentra todas calificaciones del estudiante con el id numero 4
db.grades.find({"student_id" : 4},{"score": 1});
//{ "_id" : ObjectId("50906d7fa3c412bb040eb587"), "score" : 87.89071881934647 }
//{ "_id" : ObjectId("50906d7fa3c412bb040eb588"), "score" : 27.29006335059361 }
//{ "_id" : ObjectId("50906d7fa3c412bb040eb589"), "score" : 5.244452510818443 }
//{ "_id" : ObjectId("50906d7fa3c412bb040eb58a"), "score" : 28.656451042441 }

//¿cuantos registros hay del tipo exam?
db.grades.count({"type":"exam"});
//200

//¿cuantos registros hay del tipo homework?
db.grades.count({"type":"homework"});
//400

//¿cuantos registros hay del tipo quizz?
db.grades.count({"type":"quiz"});
//200

//elimina todas las calificaciones del estudiante con el ide numero 3
db.grades.update({"student_id" : 3},{$unset: {"score":1}}, {multi:true});
//WriteResult({ "nMatched" : 4, "nUpserted" : 0, "nModified" : 4 })

//¿que estudiantes obtuvieron 75.29561445722392?
db.grades.find({"score" : 75.29561445722392});
//"_id" : ObjectId("50906d7fa3c412bb040eb59e"), "student_id" : 9, "type" : "homework", "score" : 75.29561445722392 }

// actualiza las calificaciones del registro con uuid 50906d7fa3c412bb040eb591 por 100
db.grades.update({"_id" : ObjectId("50906d7fa3c412bb040eb591")},{$set: {"score":100}}, {multi:true});
//WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })

// a que estudiabte pertenece la calificacion
db.grades.find({"_id" : ObjectId("50906d7fa3c412bb040eb591")});
//{ "_id" : ObjectId("50906d7fa3c412bb040eb591"), "student_id" : 6, "type" : "homework", "score" : 100 }
